//
//  CFRefresh.h
//  BaseComponent
//
//  Created by apple on 17/4/1.
//  Copyright © 2017年 ldm. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIScrollView;
@interface YGTableViewRefresh : NSObject

/**
 *初始化方法
 */

+ (instancetype)yg_shareRefresh;

/**
 *添加列表头部刷新
 *scrollView 需要刷新的滑动控件
 *refreshBlock 刷新的回调block
 */

- (void)yg_addHeaderRefreshToTableView:(UIScrollView *)scrollView refreshBlock:(void(^)())refreshBlock;

/**
 *添加列表底部刷新
 *scrollView 需要刷新的滑动控件
 *refreshBlock 刷新的回调block
 */

- (void)yg_addFooterRefreshToTableView:(UIScrollView *)scrollView refreshBlock:(void(^)())refreshBlock;

/**
 *结束刷新
 *scrollView 需要刷新的滑动控件
 */

- (void)yg_endRefreshToTableView:(UIScrollView *)scrollView;

@end



