//
//  UIImageView+YGWebP.h
//  YGKit
//
//  Created by apple on 17/4/12.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface UIImageView (YGWebP)

/**
 * @param url         图片的url
 */
- (void)yg_setImageWithURL:(nullable NSURL *)url;

/**
 *
 * @param url         图片的url
 * @param placeholder 未加载出url的图片时的默认图片
 */
- (void)yg_setImageWithURL:(nullable NSURL *)url
          placeholderImage:(nullable UIImage *)placeholder;



#if SD_UIKIT

#pragma mark - Animation of multiple images

/**
 * 下载一组的url数组的图片，并开始动画
 *
 * @param arrayOfURLs An array of NSURL
 */
- (void)yg_setAnimationImagesWithURLs:(nonnull NSArray<NSURL *> *)arrayOfURLs;

/**
 *取消动画
 */

- (void)yg_cancelCurrentAnimationImagesLoad;

#endif

@end
