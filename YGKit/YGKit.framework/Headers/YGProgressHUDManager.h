//
//  YGProgressHUDManager.h
//  YGKit
//
//  Created by apple on 17/4/13.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, YGProgressHUDStyle) {
    YGProgressHUDStyleLight,        // 白色背景黑色字
    YGProgressHUDStyleDark,         // 黑色背景白色字
    YGProgressHUDStyleCustom        // 透明背景黑色字
};
@class UIImage;
@interface YGProgressHUDManager : NSObject


/**
 SVProgressHUD  type:(1.success,2.error,3.info,4.custom)
 
 @param style  提示器风格
 @param type   提示器类型
 @param status 提示词
 @param time   延迟消失时间
 */
+ (void)yg_HUDSettingWithStyle:(YGProgressHUDStyle)style type:(NSInteger)type status:(NSString *)status dismissTime:(NSTimeInterval)time;

/**
 *显示
 */
+ (void)yg_show;

/**
 *status 显示提示的内容
 */

+ (void)yg_showWithStatus:(NSString *)status;

/**
 *status 显示提示内容
 */

+ (void)yg_showInfoWithStatus:(NSString *)status;

/**
 *progress 显示进度条
 */

+ (void)yg_showProgress:(float)progress;

/**
 *progress 显示进度条
 *status 显示提示内容
 */

+ (void)yg_showProgress:(float)progress status:(NSString *)status;



/**
 *成功提示
 */

+ (void)yg_showSuccessWithStatus:(NSString *)status;

/**
 *错误提示
 */

+ (void)yg_showErrorWithStatus:(NSString *)status;

/**
 *图文自定义提示
 *image 自定义的提示图片
 */

+ (void)yg_showImage:(UIImage*)image status:(NSString*)status;

/**
 *隐藏
 *time 多久隐藏,单位为秒
 */

+ (void)yg_dismissWithTime:(double)time;

@end
