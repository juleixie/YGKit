//
//  YGMenuView.h
//  4.17.MenuView
//
//  Created by ldm on 2017/4/17.
//  Copyright © 2017年 ldm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MenuViewDelegate <NSObject>

-(void)getTag:(NSInteger)tag;//获取当前被选中下标值

@end

@interface YGMenuView : UIView
{
    NSArray * _menuArray;//获取到的菜单名数组
}

-(void)setNameWithArray:(NSArray *)menuArray font:(NSInteger)font normalColor:(UIColor *)normalColor highLightColor:(UIColor *)highLightColor;//设置菜单名方法


@property(nonatomic,assign)id<MenuViewDelegate>MVDelegate;

@end
