//
//  UserZoneManager.h
//  YoGa
//
//  Created by silence on 15/7/18.
//  Copyright (c) 2015年 silence. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YGNetworkingManager : NSObject

/**
 *检测网络回调block
 *status:1.未知网络   2.无网络  3.3G/4G网络   4.wifi网络
 **/

@property (nonatomic,copy) void(^checkNetCallBack)(NSInteger status);

/**
 *初始化方法
 */
+ (instancetype)yg_shareManager;

/**
 *检测当前网络
 */

-(void)yg_checkNetwork;

/**
 *post请求方法
 *urlStr 请求路径
 *parameters 请求所提交的参数
 *success 请求成功的回调block
 *failure 请求失败的回调block
 */

- (void)yg_postJSONWithUrl:(NSString *)urlStr
             parameters:(id)parameters
                success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                   fail:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;

/**
 *get请求方法
 *urlStr 请求路径
 *parameters 请求所提交的参数
 *success 请求成功的回调block
 *failure 请求失败的回调block
 */

- (void)yg_getJSONWithUrl:(NSString *)urlStr
            parameters:(id)parameters
               success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                  fail:(void (^)(NSURLSessionDataTask *task, NSError *error))failure;
@end
