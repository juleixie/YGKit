//
//  UIButton+YGTitleImageBtn.h
//  YGKit
//
//  Created by apple on 17/4/17.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (YGTitleImageBtn)

/**
 文字图片按钮 type:(1:图片在做，文字在右)
 
 @param frame             frame
 @param backgroundColor   背景颜色
 @param title             标题
 @param titleColor        标题颜色
 @param font              字号
 @param imageName         图片名称
 @param type              文字与图片位置（1:图片在右，文字在左）
 @param layerCornerRadius 圆角
 @param alignment         内容区位置
 
 @return button
 */


+ (UIButton *)yg_titleImageButtonWithFrame:(CGRect)frame backGroundColor:(UIColor *)backgroundColor Title:(NSString *)title titleColor:(UIColor *)titleColor font:(NSInteger)font imageName:(NSString *)imageName contentType:(NSInteger)type layerCornerRadius:(CGFloat)layerCornerRadius contentHorizontalAlignment:(UIControlContentHorizontalAlignment)alignment;

@end
