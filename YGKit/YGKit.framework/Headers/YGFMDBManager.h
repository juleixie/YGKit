//
//  YGFMDBManager.h
//  FMDBDemo
//
//  Created by apple on 17/4/18.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YGFMDBManager : NSObject


/**
 初始化方法

 @return 管理的单例对象
 */
+ (instancetype)yg_shareYGFMDBManager;



/**
 根据数据model的类创建储存的表

 @param cls 需要储存数据的类

 @return 是否创建表成功
 */
- (BOOL)yg_creatTableWithClass:(Class)cls;




/**
 添加数据

 @param object 添加数据的model

 @return 是否添加成功
 */
- (BOOL)yg_insertDataWithObject:(id)object;



/**
 更新更改已有数据

 @param object     需要更新的model
 @param whereValue 根据model特有值查找更改（也就是这个属性的值）
 @param whereKey   model特有的值的key（也就是这个属性的字符串）

 @return 是否更改成功
 */
- (BOOL)yg_updateDataWithObject:(id)object whereValue:(NSString *)whereValue whereKey:(NSString *)whereKey;



/**
 移除表
 
 @param cls 需要移除的表

 @return 是否移除成功
 */
- (BOOL)yg_dropTableWithClass:(Class)cls;

/**
 删除表中所有数据

 @param cls 表的class

 @return 是否删除成功
 */
- (BOOL)yg_deleteAllDataWithClass:(Class)cls;

/**
 删除已有数据

 @param cls 需要删除数据的表
 @param whereValue 根据model特有值查找删除（也就是这个属性的值）
 @param whereKey   model特有的值的key（也就是这个属性的字符串）
 
 @return 是否删除成功
 */
- (BOOL)yg_deleteDataWithClass:(Class)cls whereValue:(NSString *)whereValue whereKey:(NSString *)whereKey;

/**
 根据需要查找的cls表来查找该model存储的所有数据

 @param cls 传入需要查找的class

 @return 所有存储的数据
 */
- (NSArray *)yg_findAllDataWithClass:(Class)cls;



/**
 根据某个属性查询相应的数据

 @param cls        需要查询的表类
 @param whereValue  属性的值
 @param whereKey   属性的key（就是属性名称的字符串）

 @return 查询的数据
 */
- (NSArray *)yg_findDataWithClass:(Class)cls whereValue:(NSString *)whereValue whereKey:(NSString *)whereKey;



@end
