//
//  YGCycleScrollView.h
//  YGKit
//
//  Created by apple on 17/4/13.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum {
    YGCycleScrollViewPageContolAlimentRight,//分页控件靠右侧
    YGCycleScrollViewPageContolAlimentCenter//分页控件在中间位置
} YGCycleScrollViewPageContolAliment;

typedef enum {
    YGCycleScrollViewPageContolStyleClassic,        // 系统自带经典样式
    YGCycleScrollViewPageContolStyleAnimated,       // 动画效果pagecontrol
    YGCycleScrollViewPageContolStyleNone            // 不显示pagecontrol
} YGCycleScrollViewPageContolStyle;


@protocol YGCycleScrollViewDelegate <NSObject>

@optional

/**
 *点击图片回调
 */
- (void)yg_didSelectItemAtIndex:(NSInteger)index;

/**
 *图片滚动回调
 */
- (void)yg_didScrollToIndex:(NSInteger)index;

@end

@interface YGCycleScrollView : UIView


/**
 *网络图片 url string 数组 
 */
@property (nonatomic, strong) NSArray *imageURLStringsGroup;

/**
 *每张图片对应要显示的文字数组 
 */
@property (nonatomic, strong) NSArray *titlesGroup;

/**
 *轮播图片的ContentMode，默认为 UIViewContentModeScaleToFill
 */
@property (nonatomic, assign) UIViewContentMode bannerImageViewContentMode;


/**
 *代理
 */

@property (nonatomic, weak) id<YGCycleScrollViewDelegate>delegate;

/**
 *自动滚动间隔时间,默认2s 
 */
@property (nonatomic, assign) CGFloat autoScrollTimeInterval;


/**
 *pagecontrol 样式，默认为动画样式
 */
@property (nonatomic, assign) YGCycleScrollViewPageContolStyle pageControlStyle;

/**
 *分页控件位置
 */
@property (nonatomic, assign) YGCycleScrollViewPageContolAliment pageControlAliment;


/**
 *是否无限循环,默认Yes
 */
@property (nonatomic,assign) BOOL infiniteLoop;


/**
 *是否自动滚动,默认Yes
 */
@property (nonatomic,assign) BOOL autoScroll;

/**
 *图片滚动方向，默认为水平滚动
 */
@property (nonatomic, assign) UICollectionViewScrollDirection scrollDirection;

/**
 *分页控件小圆标大小
 */
@property (nonatomic, assign) CGSize pageControlDotSize;

/**
 *当前分页控件小圆标颜色
 */
@property (nonatomic, strong) UIColor *currentPageDotColor;

/**
 *其他分页控件小圆标颜色
 */
@property (nonatomic, strong) UIColor *pageDotColor;

/**
 *当前分页控件小圆标图片
 */
@property (nonatomic, strong) UIImage *currentPageDotImage;

/**
 *其他分页控件小圆标图片
 */
@property (nonatomic, strong) UIImage *pageDotImage;

/**
 *轮播文字label字体颜色
 */
@property (nonatomic, strong) UIColor *titleLabelTextColor;

/**
 *轮播文字label字体大小
 */
@property (nonatomic, strong) UIFont  *titleLabelTextFont;

/**
 *轮播文字label背景颜色
 */
@property (nonatomic, strong) UIColor *titleLabelBackgroundColor;

/**
 *轮播文字label高度
 */
@property (nonatomic, assign) CGFloat titleLabelHeight;

/**
 *轮播文字label对齐方式
 */
@property (nonatomic, assign) NSTextAlignment titleLabelTextAlignment;



@end
