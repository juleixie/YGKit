//
//  YGBaseTabBarController.h
//  BaseComponent
//
//  Created by apple on 17/3/16.
//  Copyright © 2017年 ldm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YGBaseTabBarController : UITabBarController

/**
 *allInfoControllers
 *传入数组的结构为：
 *@[@[viewcontroller的名称，tabbaritem的title，普通状态的tabbaritem的图片，选中状态的tabbaritem的图片]@[viewcontroller的名称，tabbaritem的title，普通状态的tabbaritem的图片，选中状态的tabbaritem的图片]]
 *该数组里面的每一个数组为一个viewcontroller在tabbarcontroller上显示的顺序
 *navigationControllerCls 传入一个基类的头部navigationcontroller
 */

- (void)yg_configViewControllers:(NSArray *)allInfoControllers baseNavigationControllerClass:(Class)navigationControllerCls;


/**
 *normalColor tabbaritem 普通状态的字体颜色
 *selectedColor tabbaritem 选中状态的字体颜色
 *font tabbaritem的字体大小
 */

- (void)yg_configTabBarItemNormalTitleColor:(UIColor *)normalColor selectedTitleColor:(UIColor *)selectedColor font:(CGFloat)font;

@end
