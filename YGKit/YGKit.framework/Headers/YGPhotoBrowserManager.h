//
//  YGPhotoBrowserManager.h
//  CycleScrollDemo
//
//  Created by apple on 17/4/18.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <UIKit/UIKit.h>


/**
 *  图片浏览器的样式
 */
typedef NS_ENUM(NSUInteger, YGPhotoBrowserStyle){
    /**
     *  长按图片弹出功能组件,底部一个PageControl
     */
    YGPhotoBrowserStylePageControl = 1,
    /**
     * 长按图片弹出功能组件,顶部一个索引UILabel
     */
    YGPhotoBrowserStyleIndexLabel = 2,
    /**
     * 没有功能组件,顶部一个索引UILabel,底部一个保存图片按钮
     */
    YGPhotoBrowserStyleSimple = 3
};

/**
 *  pageControl的位置
 */
typedef NS_ENUM(NSUInteger, YGPhotoBrowserPageControlAliment){
    /**
     * pageControl在右边
     */
    YGPhotoBrowserPageControlAlimentRight = 1,
    /**
     *  pageControl 中间
     */
    YGPhotoBrowserPageControlAlimentCenter = 2
};

/**
 *  pageControl的样式
 */
typedef NS_ENUM(NSUInteger, YGPhotoBrowserPageControlStyle){
    /**
     * 系统自带经典样式
     */
    YGPhotoBrowserPageControlStyleClassic = 1,
    /**
     *  动画效果pagecontrol
     */
    YGPhotoBrowserPageControlStyleAnimated = 2,
    /**
     *  不显示pagecontrol
     */
    YGPhotoBrowserPageControlStyleNone = 3
    
};



@protocol YGPhotoBrowserDatasource <NSObject>

@optional
/**
 *  返回这个位置的占位图片 , 也可以是原图(如果不实现此方法,会默认使用placeholderImage)
 *
 *  @param index   位置索引
 *
 *  @return 占位图片
 */
- (UIImage *)yg_placeholderImageForIndex:(NSInteger)index;

/**
 *  返回指定位置的高清图片URL
 *
 *  @param index   位置索引
 *
 *  @return 返回高清大图索引
 */
- (NSURL *)yg_highQualityImageURLForIndex:(NSInteger)index;

/**
 *  返回指定位置图片的UIImageView,用于做图片浏览器弹出放大和消失回缩动画等
 *  如果没有实现这个方法,没有回缩动画,如果传过来的view不正确,可能会影响回缩动画
 *
 *  @param index   位置索引
 *
 *  @return 展示图片的UIImageView
 */
- (UIImageView *)yg_sourceImageViewForIndex:(NSInteger)index;

@end


@protocol YGPhotoBrowserDelegate <NSObject>

@optional

/**
 *  点击底部actionSheet回调,对于图片添加了长按手势的底部功能组件
 *  @param actionSheetindex   点击的actionSheet索引
 *  @param currentImageIndex    当前展示的图片索引
 */
- (void)yg_clickActionSheetIndex:(NSInteger)actionSheetindex currentImageIndex:(NSInteger)currentImageIndex;

@end

@interface YGPhotoBrowserManager : NSObject


/**
 *  用户点击的图片视图,用于做图片浏览器弹出的放大动画,不给次属性赋值会通过代理方法photoBrowser: sourceImageViewForIndex:尝试获取,如果还是获取不到则没有弹出放大动画
 */
@property (nonatomic, weak) UIImageView *sourceImageView;
/**
 *  当前显示的图片位置索引 , 默认是0
 */
@property (nonatomic, assign ) NSInteger currentImageIndex;
/**
 *  浏览的图片数量,大于0
 */
@property (nonatomic, assign ) NSInteger imageCount;
/**
 *  datasource
 */
@property (nonatomic, weak) id<YGPhotoBrowserDatasource> datasource;
/**
 *  delegate
 */
@property (nonatomic , weak) id<YGPhotoBrowserDelegate> delegate;
/**
 *  browser style
 */
@property (nonatomic , assign) YGPhotoBrowserStyle browserStyle;
/**
 *  占位图片,可选(默认是一张灰色的100*100像素图片)
 *  当没有实现数据源中placeholderImageForIndex方法时,默认会使用这个占位图片
 */
@property(nonatomic, strong) UIImage *placeholderImage;


#pragma mark    ----------------------
#pragma mark    自定义PageControl样式接口

/**
 *  是否显示分页控件 , 默认YES
 */
@property (nonatomic, assign) BOOL showPageControl;
/**
 *  是否在只有一张图时隐藏pagecontrol，默认为YES
 */
@property(nonatomic) BOOL hidesForSinglePage;
/**
 *  pagecontrol 样式，默认为XLPhotoBrowserPageControlStyleAnimated样式
 */
@property (nonatomic, assign) YGPhotoBrowserPageControlStyle pageControlStyle;
/**
 *  分页控件位置 , 默认为XLPhotoBrowserPageControlAlimentCenter
 */
@property (nonatomic, assign) YGPhotoBrowserPageControlAliment pageControlAliment;
/**
 *  当前分页控件小圆标颜色
 */
@property (nonatomic, strong) UIColor *currentPageDotColor;
/**
 *  其他分页控件小圆标颜色
 */
@property (nonatomic, strong) UIColor *pageDotColor;
/**
 *  当前分页控件小圆标图片
 */
@property (nonatomic, strong) UIImage *currentPageDotImage;
/**
 *  其他分页控件小圆标图片
 */
@property (nonatomic, strong) UIImage *pageDotImage;




/**
 显示浏览图片的容器(所有属性方法都要执行完这个显示，才能设置成功)

 @param images            需要浏览的图片数组
 @param currentImageIndex 当前需要显示的是第几张图片
 */
- (void)yg_showPhotoBrowserWithImages:(NSArray *)images currentImageIndex:(NSInteger)currentImageIndex;

/**
 *  初始化底部ActionSheet弹框数据
 *
 *  @param title                  ActionSheet的title
 *  @param cancelButtonTitle      取消按钮文字
 *  @param deleteButtonTitle      删除按钮文字
 *  @param otherButtonTitle      其他按钮数组(最多5个)
 */
- (void)yg_setActionSheetWithTitle:(nullable NSString *)title cancelButtonTitle:(nullable NSString *)cancelButtonTitle deleteButtonTitle:(nullable NSString *)deleteButtonTitle otherButtonTitles:(nullable NSArray *)otherButtonTitle;

/**
 *  保存当前展示的图片
 */
- (void)yg_saveCurrentShowImage;
/**
 *  进入图片浏览器
 */
- (void)yg_show;
/**
 *  退出
 */
- (void)yg_dismiss;

@end
