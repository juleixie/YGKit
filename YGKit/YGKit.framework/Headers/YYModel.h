//
//  YYModel.h
//  YYModel <https://github.com/ibireme/YYModel>
//
//  Created by ibireme on 15/5/10.
//  Copyright (c) 2015 ibireme.
//
//  This source code is licensed under the MIT-style license found in the
//  LICENSE file in the root directory of this source tree.
//

#import <Foundation/Foundation.h>

#if __has_include(<YYModel/YYModel.h>)
FOUNDATION_EXPORT double YYModelVersionNumber;
FOUNDATION_EXPORT const unsigned char YYModelVersionString[];
#import <YYModel/NSObject+YYModel.h>
#import <YYModel/YYClassInfo.h>
#else
#import "NSObject+YYModel.h"
#import "YYClassInfo.h"
#endif

/**
 *YYModel 简单用法说明
 *1.+ (nullable instancetype)yy_modelWithJSON:(id)json;
 *普通字典格式的json转换成自定义的model类的转换方法
 *
 *2.+ (nullable NSArray *)yy_modelArrayWithClass:(Class)cls json:(id)json
 *json格式为数组装的字典，可用这个方法把自定义的model类的class从cls传入，返回的数组里就是需要的自定义的model类了（注意这个方法是数组的类目，所以要用NSArray调用）
 *
 *
 *3.+ (nullable NSDictionary<NSString *, id> *)modelContainerPropertyGenericClass;
 *如果数据里字典还包含的数组或者字典，就需要在自定义的model类的.m里实现这个方法，返回一个字典，例子如下：
 
 @class YYShadow, YYBorder, YYAttachment;
 
 @interface YYAttributes
 @property NSString *name;
 @property NSArray *shadows;
 @property NSSet *borders;
 @property NSDictionary *attachments;
 @end
 
 @implementation YYAttributes
 + (NSDictionary *)modelContainerPropertyGenericClass {
 return @{@"shadows" : [YYShadow class],
 @"borders" : YYBorder.class,
 @"attachments" : @"YYAttachment" };
 }
 @end
 */
