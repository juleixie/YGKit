//
//  NSString+YGStringTool.h
//  YGKit
//
//  Created by apple on 17/4/17.
//  Copyright © 2017年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface NSString (YGStringTool)


/**
 *获取date的时间戳
 */
+ (NSString *)yg_stringWithDate:(NSDate *)date;
/**
 *时间戳转时间、日期
 */
+ (NSString *)yg_timetransform:(NSString *)timps formartStr:(NSString *)formartStr;
/**
 *时间戳转周
 */
+ (NSString*)yg_weekdayStringFromDate:(NSString*)inputStr;

/**
 *判断手机号码格式是否正确
 */
+ (BOOL)yg_valiMobile:(NSString *)mobile;

/**
 *判断text如果是@"",nil,null其中的一种就会返回YES，否则放回NO
 */

+(BOOL)yg_isEmpty:(NSString*)text;

/**
 *输入一个固定的height计算字符串的宽
 */
- (CGFloat)yg_widthWithFontSize:(CGFloat)size height:(CGFloat)height;

/**
 *输入一个固定的width计算字符串的高
 */

- (CGFloat)yg_heightWithFontSize:(CGFloat)size width:(CGFloat)width;

/**
 *base64加密
 */
+ (NSString *)yg_stringTobase64Code:(NSString *)str;

/**
 *base64解密
 */
+ (NSString *)yg_base64CodeToString:(NSString *)base64Str;


/**
 *根据当前时间转换字符串(聊天式的时间：刚刚，几分钟前，几小时前。。。)
 */

+ (NSString *)yg_compareCurrentTime:(NSString *)str;


/**
 判断当前日期是否在本周（时间的字符串如xxxx/xx/xx/ xx:xx:xx:）

 @param dateFormatter 时间的formatter格式 (对应的格式YYYY/MM/dd HH:mm:ss)

 @return 是否在本周
 */
- (BOOL)isSameWeekWithdateFormatter:(NSString *)dateFormatter;


@end
